# Design of a Surveillance System for Windows and Power Circuits of a Flat

[![pipeline status](https://inf-git.fh-rosenheim.de/ingenieurprojekte-bachelor/smartcheckhome_mhammerl/badges/main/pipeline.svg)](https://inf-git.fh-rosenheim.de/ingenieurprojekte-bachelor/smartcheckhome_mhammerl/-/commits/main)

Download latest Doxygen Document [here](https://inf-git.fh-rosenheim.de/ingenieurprojekte-bachelor/smartcheckhome_mhammerl/-/jobs/artifacts/main/raw/pdf/refman.pdf?job=pages).

# Project Summary

**Project name:** Design of a Surveillance System for Windows and Power Circuits of a Flat

**Project goal:** Create a prototypical system where the individual components (from sensors to displays) interact with each other and include adequate documentation for future students.

## Table of contents

- [Objectives](#objectives)
- [Constraints](#constraints)
- [Risks](#risks)
- [Assumptions](#assumptions)
- [Project Scope](#project-scope)
- [Milestone Chart](#milestone-chart)
- [Closing Checklist](#closing-checklist)

## Objectives:

- Scalable HW/SW structure
- High quality documentation
- Focus on code quality (portable, readable, maintainable, ...)

## Constraints:

- Possible to recreate for students in their 3rd semester (with C/C++ knowledge)
- Low total power usage
- Costs < 200€
- **Deadline:** 10.07.23

## Risks:

- Hardware availability (chip shortage) [MODEST RISK]
- Over budget [LOW RISK]
- Time [MODEST RISK]

<!-- ## Assumptions:

- List of assumptions outside the scope of the project.
- Example: Contractor will provide tools and materials
- [insert] -->

## Project Scope

### In Scope:

- Explore different approaches (HW/SW)
- prototypical interaction of individual components as a complete system
- Implementation notes (block diagrams, class diagrams, ...)

### Out of Scope:

- "Ready to sell" end product
- Follow along implementation guide

## Tasks

- [X] Set up Raspberry Pi (basic setup + MQTT broker)
- [X] Decide on final hardware setup (microcontroller and displays)
- [X] Order missing hardware
- [X] Setup network stack on microcontroller  (WiFi connection + MQTT)
- [X] Implement driver for ASK superhet receiver on microcontroller
- [X] Hardware development for current transformer
- [X] Firmware development power circuit meter
- [X] Firmware development microcontroller+display
- [X] Documentation

## Milestone Chart

| Milestone | Scope | Scheduled Completion | Actual Completion |
|-----------|-------|----------------------|-------------------|
| 1 | Set up Raspberry Pi |  | 19.03.23 |
| 2 | RPi-Pico network stack | 09.04.23 | 05.04.23 |
| 3 | Current transformer hardware | 16.04.23 | 15.05.23 |
| 4 | RF Driver | 30.04.23 | 12.04.23 |
| 5 | Current transformer firmware | 07.05.23 | 23.05.23 |
| 6 | Firmware Display | 21.05.23 | 23.06.23 |
| 7 | Documentation + finishing touches | 25.06.23 | 23.06.23 |



## Closing Checklist

- [X]  All Deliverables Checked and Tested for Quality Requirements
- [X]  Document Project (*Problems, Lessons Learned, Etc.*)
- [X]  Declare Project Completed

<!-- - [ ]  All Deliverables Checked and Tested for Quality Requirements
- [ ]  Deliver Documentation and/or Training (*If Required*)
- [ ]  Get Customer/Management/Stakeholder Sign-Off
- [ ]  Reassign Personnel, Dispose of Surplus Equipment/Materials, and Release Facilities
- [ ]  Report Final Project Status and Outcome to Customer/Management/Stakeholders -->
