extern crate rumqttc;
extern crate serde;
extern crate log;
extern crate simple_logger;

const MQTT_USER : &str = "mqtt";
const MQTT_PASS : &str = "C4C7UZhQVFWQMG";

#[derive(Debug, serde::Deserialize)]
struct Room {
    power: u32,
    windows: Vec<u32>,
}

type Rooms = std::collections::BTreeMap<String, Room>;

fn read_rooms(path: &std::path::PathBuf) -> Result<Rooms, Box<dyn std::error::Error>> {
    let contents = std::fs::read_to_string(path)?;
    let map: Rooms = serde_yaml::from_str(&contents)?;

    Ok(map)
}

fn window_id_to_room_name(id: u32, rooms: &Rooms) -> Result<String, Box<dyn std::error::Error>> {
    for (name, room) in rooms {
        if room.windows.contains(&id) {
            return Ok(name.to_string());
        }
    }

    Err(format!("No room found for window id {}", id).into())
}

fn power_id_to_room_name(id: u32, rooms: &Rooms) -> Result<String, Box<dyn std::error::Error>> {
    for (name, room) in rooms {
        if room.power == id {
            return Ok(name.to_string());
        }
    }

    Err(format!("No room found for power id {}", id).into())
}

fn main() {
    simple_logger::SimpleLogger::new().init().unwrap();

    let mut file_path = std::path::PathBuf::from("/home/pi/");
    file_path.push("map.yml");

    let rooms = match read_rooms(&file_path) {
        Ok(rooms) => rooms,
        Err(_) => panic!("Failed to read {}", file_path.display()),
    };

    rooms.iter().for_each(|(name, room)| {
        log::info!("{}: {:?}", name, room);
    });

    let mut mqtt_options = rumqttc::MqttOptions::new("mqtt-middleware", "localhost", 1883);
    mqtt_options.set_credentials(MQTT_USER, MQTT_PASS);
    mqtt_options.set_keep_alive(std::time::Duration::from_secs(5));

    let (mut client, mut connection) = rumqttc::Client::new(mqtt_options, 10);
    client
        .subscribe("/home/windows/#", rumqttc::QoS::AtMostOnce)
        .unwrap();
    client
        .subscribe("/home/power/#", rumqttc::QoS::AtMostOnce)
        .unwrap();

    // Start the rumqttc::Event loop to handle incoming messages
    for notification in connection.iter() {
        match notification {
            Ok(rumqttc::Event::Incoming(rumqttc::Packet::Publish(p))) => {
                // Process the message
                let topic = p.topic;
                let payload = p.payload;

                log::info!(
                    "Received message - Topic: {}, Payload: {:?}",
                    topic,
                    payload
                );

                if topic.starts_with("/home/power/") {
                    let id = topic.trim_start_matches("/home/power/").parse::<u32>();
                    let Ok(id) = id else {
                        log::warn!("Couldn't parse id from topic: {}.", topic);
                        continue;
                    };
                    let room_name = match power_id_to_room_name(id, &rooms) {
                        Ok(name) => name,
                        Err(_) => continue,
                    };

                    let topic = format!("/home/{}/power", room_name);
                    client
                        .publish(topic, rumqttc::QoS::AtMostOnce, true, payload)
                        .unwrap();
                }

                else if topic.starts_with("/home/windows/") {
                    let id = topic.trim_start_matches("/home/windows/").parse::<u32>();
                    let Ok(id) = id else {
                        log::warn!("Couldn't parse id from topic: {}.", topic);
                        continue;
                    };
                    let room_name = match window_id_to_room_name(id, &rooms) {
                        Ok(name) => name,
                        Err(_) => continue,
                    };

                    let topic = format!("/home/{}/windows/{}", room_name, id);
                    client
                        .publish(topic, rumqttc::QoS::AtMostOnce, true, payload)
                        .unwrap();
                }
            }
            _ => {
                log::info!("Notification: {:?}", notification);
            }
        }
    }
}
