#include "room.h"
#include <sstream>

Room::Room(const std::string& name)
  : _name(name) {
}

std::string Room::getName() const {
  return _name;
}

float Room::getPowerConsumption() const {
  return _power_consumption;
}

uint16_t Room::getOpenWindows() const {
  uint16_t num_open_windows = 0;
  for (auto& sensor : _window_sensor) {
    num_open_windows += (int)sensor.isOpen;
  }

  return num_open_windows;
}

bool Room::update_from_mqtt_message(const LwipMQTT::mqtt_message_t& message) {
  if (update_open_windows(message)) {
    return true;
  }
  else if (update_power_consumption(message)) {
    return true;
  }

  return false;
}

bool Room::update_open_windows(const LwipMQTT::mqtt_message_t& message) {
  if (message.topic.find("/windows/") == std::string::npos)
    return false;

  size_t id_pos = message.topic.find("/windows/") + std::string("/windows/").length();

  uint16_t id = std::stoi(message.topic.substr(id_pos));
  bool isOpen = std::stoi(message.payload) == 1;

  bool found = false;
  for (auto& sensor : _window_sensor) {
    if (sensor.id == id) {
      sensor.isOpen = isOpen;
      found = true;
      break;
    }
  }

  if (!found) {
    sensorData_t sensor = { id, isOpen };
    _window_sensor.push_back(sensor);
  }

  return true;
}

bool Room::update_power_consumption(const LwipMQTT::mqtt_message_t& message) {
  if (message.topic.find("/power/") == std::string::npos) {
    float pwr = std::stof(message.payload);
    _power_consumption = pwr;
    return true;
  }

  return false;
}
