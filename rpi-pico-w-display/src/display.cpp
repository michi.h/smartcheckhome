#include "display.h"

#define COLUMN(n) (n * OLED_1in3_C_WIDTH / 16)

Display::Display(/* args */) {

  if (DEV_Module_Init() != 0) {
    throw "Failed to init display...";
  }

  OLED_1in3_C_Init();
  OLED_1in3_C_Clear();

  UWORD Imagesize = ((OLED_1in3_C_WIDTH % 8 == 0) ? (OLED_1in3_C_WIDTH / 8) : (OLED_1in3_C_WIDTH / 8 + 1)) * OLED_1in3_C_HEIGHT;
  if ((_image = (UBYTE*)malloc(Imagesize)) == NULL) {
    throw "Failed to apply for black memory...";
  }

  Paint_NewImage(_image, OLED_1in3_C_WIDTH, OLED_1in3_C_HEIGHT, 0, BLACK);
}

Display::~Display() {
  free(_image);
  DEV_Module_Exit();
}

void Display::draw_home(const Home& home, const PrintType_t type) {
  Paint_SelectImage(_image);
  Paint_Clear(WHITE);
  Paint_Clear(BLACK);

  draw_header(type);

  // for each room
  uint16_t line_no = 1;
  for (size_t i = 0; i < home.rooms.size(); i++) {
    if (type == OVERVIEW && (home.rooms[i].getOpenWindows() > WINDOW_THRESHOLD ||
                             home.rooms[i].getPowerConsumption() > POWER_THRESHOLD)) {
      draw_room(home.rooms[i], line_no, type);
      line_no += 2;
    }
    else if (type == OVERVIEW) {
      continue;
    }
    else {
      draw_room(home.rooms[i], line_no, type);
      line_no++;
    }
  }

  OLED_1in3_C_Display(_image);
}

void Display::draw_header(const PrintType_t type) {
  Paint_DrawString_EN(COLUMN(0), 3, "RAUM", &Font8, WHITE, BLACK);

  switch (type) {
  case WINDOWS:
    Paint_DrawString_EN(COLUMN(8), 3, "FENSTER", &Font8, WHITE, BLACK);
    break;

  case POWER:
    Paint_DrawString_EN(COLUMN(8), 3, "LEISTUNG [W]", &Font8, WHITE, BLACK);
    break;
    
  default:
    break;
  }

}

void Display::draw_room(const Room& room, const uint16_t line_no, const PrintType_t type) {
  Paint_DrawString_EN(COLUMN(0), get_line_px(line_no), room.getName().c_str(), &Font8, WHITE, BLACK);

  switch (type) {
  case WINDOWS:
    Paint_DrawString_EN(COLUMN(8), get_line_px(line_no), std::to_string(room.getOpenWindows()).c_str(), &Font8, WHITE, BLACK);
    break;

  case POWER:
    Paint_DrawString_EN(COLUMN(8), get_line_px(line_no), std::to_string(room.getPowerConsumption()).c_str(), &Font8, WHITE, BLACK);
    break;

  case OVERVIEW:
    Paint_DrawString_EN(COLUMN(8), get_line_px(line_no), std::to_string(room.getOpenWindows()).c_str(), &Font8, WHITE, BLACK);
    Paint_DrawString_EN(COLUMN(8), get_line_px(line_no + 1), std::to_string(room.getPowerConsumption()).c_str(), &Font8, WHITE, BLACK);
    break;

  default:
    break;
  }
}

uint16_t Display::get_line_px(uint8_t line_no) const {
  return (3 + (line_no * OLED_1in3_C_HEIGHT / 8));
}
