#include "constants.h"
#include "lwip_mqtt.h"
#include "home.h"
#include "display.h"
#include "pico/cyw43_arch.h"
#include "pico/stdlib.h"

#include <string>

volatile Display::PrintType_t print_type = Display::OVERVIEW;

void gpio_callback(uint gpio, uint32_t events) {
  // cycle through print types
  print_type = static_cast<Display::PrintType_t>((static_cast<int>(print_type) + 1) %
                                                 (static_cast<int>(Display::POWER) + 1));
}

void blink_led();

int main() {
  stdio_init_all();

  cyw43_arch_init_with_country(CYW43_COUNTRY_GERMANY);
  cyw43_arch_enable_sta_mode();
  cyw43_wifi_pm(&cyw43_state, CYW43_PERFORMANCE_PM);

  LwipMQTT mqtt = LwipMQTT(
      "PiPicoDisplay",  /* client id*/
      "mqtt",           /* user */
      "C4C7UZhQVFWQMG"  /* pass */
  );

  Home home;

  Display display;

  // GPIO 15 is Waveshare Pico OLED 1.3 KEY0
  gpio_set_irq_enabled_with_callback(15, GPIO_IRQ_EDGE_RISE, true, &gpio_callback);

  while (true) {

    if (cyw43_wifi_link_status(&cyw43_state, CYW43_ITF_STA) != CYW43_LINK_JOIN)
      cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASS, CYW43_AUTH_WPA2_AES_PSK, WIFI_CONNECT_TIMEOUT_MS);

    if (!mqtt.is_connected())
      mqtt.connect(MQTT_BROKER_IP, 1883);

    bool update = false;
    LwipMQTT::mqtt_message_t message;
    while (mqtt.message_is_available()) {
      message = mqtt.read_message();

      printf("MQTT Message: \n- %s - %s\r\n",
        message.topic.c_str(),
        message.payload.c_str());

      update = home.update_rooms_from_mqtt(message);
    }

    display.draw_home(home, print_type);

    blink_led();
  }

  return 0;
}

void blink_led() {
  cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
  sleep_ms(250);
  cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
  sleep_ms(750);
}
