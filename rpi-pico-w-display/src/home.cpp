#include "home.h"
#include <regex>

bool Home::update_rooms_from_mqtt(const LwipMQTT::mqtt_message_t& message) {
  // extract room from topic "/home/ROOM/#"
  std::regex regex("/home/([^/]+)");
  std::smatch match;

  Room* room_to_update;

  if (!std::regex_search(message.topic, match, regex))
    return false;

  const std::string room_mqtt = match[1];

  // search for room in vector
  bool found = false;
  for (auto& room : rooms) {
    if (room == room_mqtt) {
      room_to_update = &room;
      found = true;
      break;
    }
  }

  // create new room if not found
  if (!found) {
    Room new_room = Room(room_mqtt);
    rooms.push_back(new_room);
    room_to_update = &rooms.back();
  }

  return room_to_update->update_from_mqtt_message(message);
}
