/**
 * @file room.h
 * @author Michael Hammerl (michael.hammerl@stud.th-rosenheim.de)
 * @brief Room class, holds power consumption and open windows
 * @version 0.1
 * @date 2023-05-08
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef ROOM_H
#define ROOM_H

#include "lwip_mqtt.h"
#include "sensor_data.h"
#include <string>
#include <vector>
#include <cstdint>

/**
 * @brief holds the power consumption and open windows for a room
 *
 */
class Room {
public:
  Room(const std::string& name);

  /**
   * @brief Get the Room name
   *
   * @return std::string
   */
  std::string getName() const;

  /**
   * @brief Get the Room power consumption
   *
   * @return float
   */
  float getPowerConsumption() const;

  /**
   * @brief Get the number of open windows
   *
   * @return uint16_t
   */
  uint16_t getOpenWindows() const;

  /**
   * @brief Updates the room from an mqtt message
   *
   * @param message MQTT message to update from
   * @return true room updated
   * @return false no update needed
   */
  bool update_from_mqtt_message(const LwipMQTT::mqtt_message_t& message);

  bool operator==(const Room& a) const {
    return (_name == a.getName());
  }

  bool operator==(const std::string& a) const {
    return (_name == a);
  }

private:


  /**
   * @brief parses mqtt message to update power consumption
   *
   * @param message
   * @return true successful
   * @return false "/home/power/" not found in topic string
   */
  bool update_power_consumption(const LwipMQTT::mqtt_message_t& message);

  /**
   * @brief parses mqtt message to update _window_sensor
   *
   * @param message
   * @return true successful
   * @return false "/home/windows/" not found in topic string
   */
  bool update_open_windows(const LwipMQTT::mqtt_message_t& message);

  const std::string _name;
  float _power_consumption;
  std::vector<sensorData_t> _window_sensor;
};

#endif /* ROOM_H */
