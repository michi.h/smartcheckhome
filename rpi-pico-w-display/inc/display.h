/**
 * @file display.h
 * @author Michael Hammerl (michael.hammerl@stud.th-rosenheim.de)
 * @brief OLED Abstraction class to display information from Home
 * @version 0.1
 * @date 2023-05-08
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include "home.h"
#include "room.h"

// enable C-library linkage (stop compiler from mangling function names)
extern "C" {
#include "DEV_Config.h"
#include "OLED_1in3_c.h"
#include "GUI_Paint.h"
}

#include <cstdint>

#define POWER_THRESHOLD 50.0
#define WINDOW_THRESHOLD 0

class Display
{
public:
  Display(/* args */);
  ~Display();

  /**
   * @brief decide weather to print power usage or open windows
   *
   */
  typedef enum {
    OVERVIEW,
    WINDOWS,
    POWER
  } PrintType_t;

  /**
   * @brief Draw each Home room's data on Display
   *
   * @param home
   */
  void draw_home(const Home& home, const PrintType_t type);

private:
  UBYTE* _image;

  void draw_room(const Room& room, const uint16_t line_no, const PrintType_t type);
  void draw_header(const PrintType_t type);
  uint16_t get_line_px(uint8_t line_no) const;
};

#endif /* DISPLAY_H */
