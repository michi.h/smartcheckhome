/**
 * @file home.h
 * @author Michael Hammerl (michael.hammerl@stud.th-rosenheim.de)
 * @brief Home class, holds rooms and updates them from mqtt messages
 * @version 0.1
 * @date 2023-05-08
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef HOME_H
#define HOME_H

#include "room.h"
#include "lwip_mqtt.h"

#include <vector>
#include <string>

class Home {
public:
  /**
   * @brief Updates the rooms from an mqtt message and creates a new room if it's not present already
   *
   * @param message MQTT message to update rooms from
   * @return true room updated
   * @return false no update needed
   */
  bool update_rooms_from_mqtt(const LwipMQTT::mqtt_message_t& message);

  /**
   * @brief Holds the rooms of the home.
   *
   */
  std::vector<Room> rooms;

private:

};

#endif /* HOME_H */
