# Design of a Surveillance System for Windows and Power Circuits of a Flat

## Used Hardware

- Raspberry Pi Zero W (MQTT Broker, MQTT Middleware)
- Raspberry Pi Pico W (ASK RF Receiver)
- Raspberry Pi Pico W (Current sensor)
- Raspberry Pi Pico W (Display)

![diagram](diagram-overview.drawio.png)

# Raspberry Pi Setup w/ Mosquitto MQTT Broker and Docker

- Create image with Raspberry Pi Imager (enable ssh, create user, wifi config)
- create docker-compose.yml for mosquitto image

```yml
version: "3.8"
services:
    mosquitto:
        image: eclipse-mosquitto
        container_name: mosquitto
        restart: unless-stopped
        volumes:
        - ~/docker/mosquitto/data:/mosquitto/data
        - ~/docker/mosquitto/log:/mosquitto/log
        - ~/docker/mosquitto/config:/mosquitto/config
        ports:
        - 1883:1883
        - 9001:9001
```

```bash
# install docker
sudo apt install docker

# start Docker service at system boot
sudo systemctl enable --now docker

# start the container
sudo docker-compose up -d
```

*~/docker/mosquitto/config/mosquitto.conf*

    listener 1883
    password_file /mosquitto/config/passwd

create *~/docker/mosquitto/config/passwd*

```bash
# enter container console
docker exec -it mosquitto sh

# create password file withing the container
mosquitto_passwd -c /mosquitto/config/passwd <user>
```

This will ask you for a password and will create the file *passwd* in */mosquitto/config/passwd* with the user <user> and the password you provided in a hashed form. Adjust <path> and <user> to your needs. Be sure this file can be read by the user mosquitto runs. If you are adding a new user to an existing password file, you can omit the ‘-c’ parameter.


# Raspberry Pi Pico

## Toolchain


- [openocd](https://github.com/earlephilhower/pico-quick-toolchain/releases/) -> enable on-chip debugging. For Windows: openocd.win64

- [arm-none-eabi](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads/product-release) -> cross-compiler. For Windows: gcc-arm-none-eabi-10-2020-q4-major-win32.exe

- [mingw64](https://sourceforge.net/projects/mingw-w64/files/mingw-w64/mingw-w64-release/) -> c++ compiler etc. maybe optional?  For Windows: x86_64-posix-sjlj
- [cmake](https://cmake.org/download/) -> generate platform specific build files
- [ninja](https://github.com/ninja-build/ninja/releases) -> execute build

Unpack/Install to e.g. "C:/VSARM/.." and add bin folders to PATH Environment Variable.

The Pico-SDK will be pulled automatically when first building the project using CMake.

## Debugging

- [on chip debugging](https://github.com/majbthrd/pico-debug)

Uses the 2nd Core of the Pi Pico as a debug probe for the 1st Core. This allows you to debug your code using gdb and openocd.
Downside: USB to UART bridge is not available anymore.

- [picoprobe](https://github.com/raspberrypi/picoprobe)

Lets you use a Raspberry Pi Pico as a debug probe for other RP2040 boards by implementing a USB -> SWD and UART bridge.

### Connection

| Picoprobe    | Target       |
|--------------|--------------|
| VSYS         | 5V           |
| GND          | GND          |
| GP2          | SWCLK        |
| GP3          | SWDIO        |
| GP4/UART1 Tx | GP1/UART0 Rx |
| GP5/UART1 Rx | GP0/UART0 Tx |

## LwipMQTT Class

Abstraction layer for the lwIp (lightweight IP) [MQTT Module](https://www.nongnu.org/lwip/2_0_x/group__mqtt.html).

Important lwipopts.h (that differ from the standard settings):

```C
/* You need to increase MEMP_NUM_SYS_TIMEOUT by one if you use MQTT!
 * see https://forums.raspberrypi.com/viewtopic.php?t=341914
 */
#define MEMP_NUM_SYS_TIMEOUT   (LWIP_NUM_SYS_TIMEOUT_INTERNAL + 1)
#define MQTT_REQ_MAX_IN_FLIGHT  (5) /* maximum of subscribe requests */
```

## ASK Receiver driver

<!-- ```C
// describe high and low time of a single bit in us
typedef struct {
    uint16_t high;
    uint16_t low;
} highLow_t;

typedef struct {
    highLow_t zero;
    highLow_t one;
    uint16_t syncLength;  // lenth of low level before data starts in us
    uint16_t numOfBits;
} protocol_t;

static constexpr protocol_t _protocol {
    { 525, 1325 }, // zero
    { 1425, 425 }, // one
    14000,         // syncLength
    24             // numOfBits -> throw away last bit (0) -> causes problems because of the sync pulse afterwards
};
```
-->

### Protocol Struct Diagram

```plantuml
@startuml
left to right direction

class highLow_t {
  + high: uint16_t
  + low: uint16_t
}

class protocol_t {
  + zero: highLow_t
  + one: highLow_t
  + syncLength: uint16_t
  + numOfBits: uint16_t
}

object _protocol {
  + zero = { 525, 1325 }
  + one = { 1425, 425 }
  + syncLength = 14000
  + numOfBits = 24
}

highLow_t "1" -- protocol_t : "zero"
highLow_t "1" -- protocol_t : "one"
protocol_t -- _protocol

@enduml
```

### Basic idea

- use an interrupt to measure the time between two edges
- save last and current duration
- if the duration is in the range of the sync pulse -> start listening for data
- if the duration is in the range of a zero or one -> save the bit
- else -> throw away the data and start listening for sync pulse again
- if numOfBits is reached -> parse and save the data and start listening for sync pulse again

### sensor open

```
100101 000100000 100001 0 100 *ID1*
100101 011011101 100001 0 100 *ID2*
100101 100010110 100001 0 100 *ID3*
```

### sensor close

```
100101 000100000 100001 1 100 *ID1*
```

### resulting id's as int

```
0b000100000 = 32
0b011011101 = 221
0b100010110 = 278
```

## Current transformer / ADS1115 + Pi Pico

### Burden Resistance Calculations

impedance of secondary windings of CT not considered

Data sheet ZMCT116A
-> turns ratio 2500:1 -> Ns = 2500

```
R = (Var  *  Ns)   /  (2√2  * Ipmax)
R = (5 V  *  2500) /  (2√2  * 20 A) = 220 Ω

where
R     = Burden resistance
Var   = Analog reference voltage
Ns    = Num of turns in secondary winding
Ipmax = Maximum primary current
```

### Conversion factor calculation

```
Ipmax = 20 A ≙ 2.5 Vp = 2.5/√2 Vrms (result of calculated burden resistor value)
=> k = 20 A / 2.5/√2 V = 8√2 A/V
=> P = Vadc * 8√2 A/V * 230 V
```

### Class Diagram

```plantuml
@startuml
class ADC {
  - _mux: ads1115_mux_t[4]
  - _adc: struct ads1115_adc
  - _sensors: std::array<std::unique_ptr<Sensor>, 4>
  - _num_of_sensors: uint8_t
  + ADC(num_of_inputs: uint8_t, sda_pin: uint8_t, scl_pin: uint8_t, i2c_addr: uint8_t, i2c_port: i2c_inst_t*)
  + ~ADC()
  + read(value: std::array<float, 4>&): void
  + get_num_of_sensors(): uint8_t
}

class Sensor {
  - _adc: ads1115_adc_t*
  - _mux: ads1115_mux_t
  - _dc_offset: volatile inline static uint16_t
  - _buf: inline static std::array<uint16_t, NUM_OF_SAMPLES>
  - _new_value_rdy: volatile inline static bool
  - _adc_v_rms: volatile inline static double
  + Sensor(adc: ads1115_adc_t&, mux: ads1115_mux_t)
  + read(): double
}

ADC "n " *-- "1..4 " Sensor
@enduml
```

### PCB

![shematic](./current%20transformer%20pcb/Schematic_CT_interface_2023-06-03.png)
![pcb](./current%20transformer%20pcb/Fotoansicht_2023-06-03.png)

Files for EasyEDA and manufacturing can be found at "./current transformer pcb/"

## Display

[documentation](https://www.waveshare.com/wiki/Pico-OLED-1.3)

### States

```plantuml
@startuml
left to right direction

[*] --> Overview
Overview : Display Rooms with power consumption or open windows  over threshold

Overview --> Windows : button press
Windows : Display open windows for each room

Windows --> Power : button press
Power : Display power consumption for each room

Power --> Overview : button press

@enduml
```

# MQTT Middleware

## Basic idea

- Assigns power usage and window sensor id's to rooms according to a config file (/home/pi/map.yml).

## Run app at startup

1. Build rust app
```
cargo build --release
```

2. Copy binary to /usr/bin/rust-mqtt-middleware
```
sudo cp target/release/rust-mqtt-middleware /usr/bin/rust-mqtt-middleware
```

3. Create systemd service file
```
sudo nano /etc/systemd/system/rust-mqtt-middleware.service
```
```
[Unit]
Description=rust-mqtt-middleware
After=network-online.target

[Service]
Type=simple
Restart=always
RestartSec=1
ExecStart=/usr/bin/rust-mqtt-middleware

[Install]
WantedBy=multi-user.target
```

4. Enable and start service
```
sudo systemctl daemon-reload
sudo systemctl enable rust-mqtt-middleware.service
sudo systemctl start rust-mqtt-middleware
```

5. check status
```
sudo systemctl status rust-mqtt-middleware
```
