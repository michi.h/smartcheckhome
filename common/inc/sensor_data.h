/**
 * @file sensor_data.h
 * @author Michael Hammerl (michael.hammerl@stud.th-rosenheim.de)
 * @brief
 * @version 0.1
 * @date 2023-05-10
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef SENSOR_DATA_H
#define SENSOR_DATA_H

#include <cstdint>

// holds the ID and open/close State of a generic rf window sensor
typedef struct sensorData {
  uint16_t id;
  bool isOpen;

  bool operator==(const sensorData& a) const {
    return (id == a.id && isOpen == a.isOpen);
  }
} sensorData_t;


#endif /* SENSOR_DATA_H */
