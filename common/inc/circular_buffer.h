#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <cstdint>
#include <string>
#include <vector>

constexpr size_t STD_BUFFER_SIZE = 64;

class CircularBuffer {
public:
  CircularBuffer();
  CircularBuffer(size_t size);

  bool isEmpty() const;

  bool isFull() const;

  /**
   * @brief Write string to buffer tail
   *
   * @param str
   */
  void write(const std::string& str);

  /**
   * @brief Read string from buffer head
   *
   * @return std::string: "" if buffer is empty
   */
  std::string read();

private:
  std::vector<std::string> _buffer;
  uint16_t _head;
  uint16_t _tail;
  bool _is_full;
};

#endif // CIRCULAR_BUFFER_H
