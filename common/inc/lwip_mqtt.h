/**
 * @file lwip_mqtt.h
 * @author Michael Hammerl (michael.hammerl@stud.th-rosenheim.de)
 * @brief
 * @version 0.1
 * @date 2023-04-20
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef LWIP_MQTT_H
#define LWIP_MQTT_H

#include "lwip/apps/mqtt.h"
#include "circular_buffer.h"
#include <string>
#include <array>

#define MQTT_BROKER_IP IPADDR4_INIT_BYTES(192, 168, 137, 219)

constexpr size_t BUFFER_SIZE = 1024;

class LwipMQTT
{
public:
  /**
   * @brief Construct a new LwipMQTT object
   *
   * @param id Client ID (has to be uinique)
   * @param user broker user
   * @param passwd broker password
   */
  LwipMQTT(const char* id, const char* user, const char* passwd);
  ~LwipMQTT();

  inline static constexpr std::array<const char*, 2> TOPICS = {
    "/home/+/windows/#",
    "/home/+/power/#"
  };

  typedef struct {
    std::string topic;
    std::string payload;
  } mqtt_message_t;

  /**
   * @brief Connect to MQTT Broker.
   *
   * @param ip: const ip_addr_t
   * @param port: const uint16_t
   * @return err_t
   */
  err_t connect(const ip_addr_t& ip, const uint16_t port);

  /**
   * @brief Publish message to a topic.
   *
   * @param topic: Topic to publish to
   * @param payload: Message payload
   * @return err_t
   */
  err_t publish(const std::string& topic, const std::string& payload);

  /**
   * @brief Subscribes to a single topic.
   *
   * @param topic: Topic to subscribe to
   * @param qos: Quality of service, 0 1 or 2
   * @return err_t
   */
  err_t subscribe(const char* topic, const uint8_t qos);

  /**
   * @brief Unsubscribe to a single topic.
   *
   * @param topic Topic to unsubscribe
   * @return err_t
   */
  err_t unsubscribe(const char* topic);

  /**
   * @brief Check if client is connected to broker.
   *
   * @return true
   * @return false
   */
  bool is_connected();

  /**
   * @brief Check if a new message is available
   *
   * @return true
   * @return false
   */
  bool message_is_available();

  /**
   * @brief Read a single message from buffer
   *
   * @return mqtt_message_t
   */
  mqtt_message_t read_message();

private:
  mqtt_client_t* _client_instance;
  struct mqtt_connect_client_info_t _client_info;

  // callbacks for lqip mqtt module

  static void connection_cb(mqtt_client_t* client, void* arg, mqtt_connection_status_t status);
  static void sub_request_cb(void* arg, err_t err);
  static void incoming_publish_cb(void* arg, const char* topic, uint32_t tot_len);
  static void incoming_data_cb(void* arg, const uint8_t* data, uint16_t len, uint8_t flags);

  inline static CircularBuffer _topic_buffer;
  inline static CircularBuffer _payload_buffer;
};

#endif /* LWIP_MQTT_H */
