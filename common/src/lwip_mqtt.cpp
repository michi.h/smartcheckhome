#include "lwip_mqtt.h"
#include "pico/cyw43_arch.h"

LwipMQTT::LwipMQTT(const char* id, const char* user, const char* passwd )
  : _client_instance{ mqtt_client_new() }
  , _client_info{
      id,
      user,
      passwd,
      100,  /* keep alive */
      NULL, /* will_topic */
      NULL, /* will_msg */
      0,    /* will_qos */
      0     /* will_retain */
  #if LWIP_ALTCP && LWIP_ALTCP_TLS
    , NULL
  #endif
  } {
  mqtt_set_inpub_callback(
    _client_instance,
    incoming_publish_cb,
    incoming_data_cb,
    LWIP_CONST_CAST(void*, &_client_info));
}

LwipMQTT::~LwipMQTT() {
  cyw43_arch_lwip_begin();
  mqtt_disconnect(_client_instance);
  mqtt_client_free(_client_instance);
  cyw43_arch_lwip_end();
}

err_t LwipMQTT::connect(const ip_addr_t& ip, const uint16_t port) {

  cyw43_arch_lwip_begin(); /* start section for to lwIP access */

  err_t err = mqtt_client_connect(
            _client_instance,
            &ip,
            port,
            connection_cb,
            LWIP_CONST_CAST(void*, &_client_info),
            &_client_info);

  cyw43_arch_lwip_end(); /* end section accessing lwIP */

  return err;
}

err_t LwipMQTT::publish(const std::string& topic, const std::string& payload) {
  return mqtt_publish(
    _client_instance,
    topic.c_str(),
    payload.c_str(),
    payload.size() + 1,
    1,
    1,
    sub_request_cb,
    LWIP_CONST_CAST(void*, &_client_info));
}

bool LwipMQTT::is_connected() {
  return mqtt_client_is_connected(_client_instance);
}

bool LwipMQTT::message_is_available() {
  return !_payload_buffer.isEmpty();
}

LwipMQTT::mqtt_message_t LwipMQTT::read_message() {

  return { _topic_buffer.read(), _payload_buffer.read() };
}

err_t LwipMQTT::subscribe(const char* topic, const uint8_t qos) {
  return mqtt_sub_unsub(
    _client_instance,
    topic,
    qos,
    sub_request_cb,
    LWIP_CONST_CAST(void*, &_client_info),
    1
  );
}

err_t LwipMQTT::unsubscribe(const char* topic) {
  return mqtt_sub_unsub(
    _client_instance,
    topic,
    1,
    sub_request_cb,
    LWIP_CONST_CAST(void*, &_client_info),
    0
  );
}

void LwipMQTT::connection_cb(mqtt_client_t* client, void* arg, mqtt_connection_status_t status) {
  const struct mqtt_connect_client_info_t* client_info = (const struct mqtt_connect_client_info_t*)arg;
  LWIP_UNUSED_ARG(client);

  LWIP_PLATFORM_DIAG(("MQTT client \"%s\" connection cb: status %d\n", client_info->client_id, (int)status));

  if (status == MQTT_CONNECT_ACCEPTED) {
    for (auto topic : TOPICS) {
      mqtt_sub_unsub(
      client,
      topic,
      0,
      sub_request_cb,
      LWIP_CONST_CAST(void*, client_info),
      1
      );
    }
  }
}

void LwipMQTT::sub_request_cb(void* arg, err_t err) {
  const struct mqtt_connect_client_info_t* client_info = (const struct mqtt_connect_client_info_t*)arg;

  LWIP_PLATFORM_DIAG(("MQTT client \"%s\" request cb: err %d\n", client_info->client_id, (int)err));
}

void LwipMQTT::incoming_publish_cb(void* arg, const char* topic, uint32_t tot_len) {
  const struct mqtt_connect_client_info_t* client_info = (const struct mqtt_connect_client_info_t*)arg;

  LWIP_PLATFORM_DIAG(("MQTT client \"%s\" publish cb: topic %s, len %d\n", client_info->client_id,
    topic, (int)tot_len));

  std::string str_topic = (const char*)topic;
  _topic_buffer.write(str_topic);
}

void LwipMQTT::incoming_data_cb(void* arg, const uint8_t* data, uint16_t len, uint8_t flags) {
  const struct mqtt_connect_client_info_t* client_info = (const struct mqtt_connect_client_info_t*)arg;

  LWIP_PLATFORM_DIAG(("MQTT client \"%s\" data cb: len %d, flags %d\n", client_info->client_id,
    (int)len, (int)flags));

  std::string str_data = (const char*)data;
  _payload_buffer.write(str_data);
}
