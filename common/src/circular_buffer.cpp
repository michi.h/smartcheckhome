#include "circular_buffer.h"

CircularBuffer::CircularBuffer()
  : _buffer(STD_BUFFER_SIZE)
  , _head(0)
  , _tail(0)
  , _is_full(false) {
}

CircularBuffer::CircularBuffer(size_t size)
  : _buffer(size)
  , _head(0)
  , _tail(0)
  , _is_full(false) {
}

bool CircularBuffer::isEmpty() const {
  return !_is_full && (_head == _tail);
}

bool CircularBuffer::isFull() const {
  return _is_full;
}

void CircularBuffer::write(const std::string& str) {
  _buffer[_tail] = str;
  if (_is_full) {
    _head = (_head + 1) % _buffer.size();
  }
  _tail = (_tail + 1) % _buffer.size();
  _is_full = (_tail == _head);
  if (_is_full) {
    _head = (_head + 1) % _buffer.size();
  }
}

std::string CircularBuffer::read() {
  if (isEmpty()) {
    return "";
  }
  std::string val = _buffer[_head];
  _head = (_head + 1) % _buffer.size();
  _is_full = false;
  return val;
}
