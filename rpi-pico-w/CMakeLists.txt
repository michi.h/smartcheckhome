cmake_minimum_required(VERSION 3.17)

set(PICO_BOARD pico_w CACHE STRING "Board type")

# Pull in Raspberry Pi Pico SDK (must be before project)
set(PICO_SDK_FETCH_FROM_GIT on)
include(pico_sdk_import.cmake)

set(PROJECT_NAME "pico-w-project")
project(${PROJECT_NAME} C CXX ASM)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_BUILD_TYPE "Debug")
set(PICO_DEOPTIMIZED_DEBUG ON)

# Path to the pico-sdk directory
# set(PICO_SDK_PATH ${CMAKE_SOURCE_DIR}/third-party/pico-sdk)
# Path to your source files
set(SRC_PATH ${CMAKE_SOURCE_DIR}/src)
# Path to your header files
set(INC_PATH ${CMAKE_SOURCE_DIR}/inc)

# add common folder
add_subdirectory(../common common)

# Initialize the pico-sdk
pico_sdk_init()

# Add your source files
add_executable(${PROJECT_NAME}
    ${SRC_PATH}/main.cpp
    ${SRC_PATH}/rf_ask_driver.cpp
)

# Add your header files
target_include_directories(${PROJECT_NAME} PRIVATE
    ${INC_PATH}
)

pico_enable_stdio_uart(${PROJECT_NAME} 1)
pico_enable_stdio_usb(${PROJECT_NAME} 0)

# Add the PICO SDK libraries
target_link_libraries(${PROJECT_NAME}
    pico_stdlib
    pico_cyw43_arch_lwip_threadsafe_background
    common
    # pico_lwip_mbedtls
    # pico_lwip_mqtt
    # pico_mbedtls
    # hardware_timer
    # hardware_i2c
    # hardware_spi
    # hardware_dma
    # hardware_pio
    # hardware_interp
    # hardware_watchdog
    # hardware_clocks
)

# Set the output file name
set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME ${PROJECT_NAME})

# Create a .elf and .uf2 files
pico_add_extra_outputs(${PROJECT_NAME})
