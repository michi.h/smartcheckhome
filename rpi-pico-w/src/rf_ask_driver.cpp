#include "rf_ask_driver.h"
#include "pico/stdlib.h"

#include <algorithm>

RfAskDriver::RfAskDriver(const uint16_t rxPin)
  : _rxPin{ rxPin } {
  gpio_init(rxPin);
  gpio_set_dir(rxPin, GPIO_IN);
}

RfAskDriver::~RfAskDriver() {
  stopReceiving();
  gpio_deinit(this->_rxPin);
}

bool RfAskDriver::newDataAvailable() {
  return _sensorDataBuf.size();
}

sensorData_t RfAskDriver::popDataFromBuffer() {
  sensorData_t ret = { 0,true };

  if (newDataAvailable()) {
    ret = _sensorDataBuf.front();
    _sensorDataBuf.pop();
  }

  return ret;
}

bool RfAskDriver::areEqualWithTolerance(const uint32_t& a, const uint32_t& b) {
  uint32_t diff = abs(a - b);
  uint32_t maxVal = std::max(a, b);
  uint32_t tolerance = (maxVal * _receivingTolerance) >> 8;
  return diff <= tolerance;
}

bool RfAskDriver::areEqualWithTolerance(const highLow_t& a, const highLow_t& b) {
  uint16_t diffHigh = abs(a.high - b.high);
  uint16_t maxValHigh = std::max(a.high, b.high);
  uint32_t toleranceHigh = (maxValHigh * _receivingTolerance) >> 8;

  uint16_t diffLow = abs(a.low - b.low);
  uint16_t maxValLow = std::max(a.low, b.low);
  uint32_t toleranceLow = (maxValLow * _receivingTolerance) >> 8;

  return ((diffHigh <= toleranceHigh) + (diffLow <= toleranceLow));
}

/**
 * Data in _rxBuf[]
 *           id          isOpen
 * ------ XXXXXXXXX ------ X ---
 *
 */
void RfAskDriver::parseBuffer() {
  sensorData_t newData = { 0, true };

  newData.isOpen = !_rxBuf.test(21);

  // iteration from MSB down to LSB
  // the resulting uint is constructed with the bits in the correct order
  for (int i = 8; i >= 0; i--) {
    newData.id |= _rxBuf.test(i + 6) << (8 - i);
  }

  if (_sensorDataBuf.size() > BUF_LEN)
    _sensorDataBuf.pop();

  _sensorDataBuf.push(newData);
  _rxBuf.reset();
}


/**
 * @brief Interrupt handler for receiving PWM-Data from RF Receiver
 *
 *
 */
void RfAskDriver::handleInterrupt(uint gpio, uint32_t events) {
/**
 *  Protocol always starts with low
 *
 *                 ________    __                 __
 * _______________|        |__|  |________|XXXXX_|  |__
 *
 * |--syncLength--|--one------|--zero-----|         |--syncLength--
 */
  static uint32_t lastTime = 0;
  static uint32_t lastDuration = 0;
  static bool isListening = false;

  static uint16_t bufIdx = 0;

  const uint32_t time = time_us_32();
  const uint32_t duration = time - lastTime;

  highLow_t timing;
  if (isListening && events == GPIO_IRQ_EDGE_RISE) {
    // rising edge -> high/low sequence done -> bit completed
    timing.low = duration;
    timing.high = lastDuration;

    _timings[bufIdx] = timing;

    if (areEqualWithTolerance(timing, _protocol.one)) {
      _rxBuf[bufIdx] = 1;
      bufIdx++;
    }
    else if (areEqualWithTolerance(timing, _protocol.zero)) {
      _rxBuf[bufIdx] = 0;
      bufIdx++;
    }
    else {
      // error receiving
      isListening = false;
    }
  }

  if (areEqualWithTolerance(duration, _protocol.syncLength)) {
    // sync signal received -> start listening
    isListening = true;
    _rxBuf.reset();
    _timings.fill({ 0,0 });
    bufIdx = 0;
  }

  if (bufIdx >= _protocol.numOfBits) {
    parseBuffer();
    isListening = false;
    bufIdx = 0;
  }

  lastDuration = duration;
  lastTime = time;
}


void RfAskDriver::startReceiving() {
  gpio_set_irq_enabled_with_callback(this->_rxPin, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &handleInterrupt);
}

void RfAskDriver::stopReceiving() {
  gpio_set_irq_enabled_with_callback(this->_rxPin, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, false, &handleInterrupt);
}
