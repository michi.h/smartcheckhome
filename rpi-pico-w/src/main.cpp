#include "constants.h"
#include "rf_ask_driver.h"
#include "lwip_mqtt.h"
#include "sensor_data.h"
#include "pico/cyw43_arch.h"
#include "pico/stdlib.h"

#include <string>

const uint8_t RF_RX_PIN = 22;

void blink_led();

int main() {
  stdio_init_all();

  cyw43_arch_init_with_country(CYW43_COUNTRY_GERMANY);
  cyw43_arch_enable_sta_mode();
  cyw43_wifi_pm(&cyw43_state, CYW43_PERFORMANCE_PM);

  LwipMQTT mqtt = LwipMQTT(
      "PiPicoRF",       /* client id*/
      "mqtt",           /* user */
      "C4C7UZhQVFWQMG"  /* pass */
  );

  RfAskDriver rf_recv = RfAskDriver(RF_RX_PIN);
  rf_recv.startReceiving();

  sensorData_t lastSensorData = {0,0};

  cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASS, CYW43_AUTH_WPA2_AES_PSK, WIFI_CONNECT_TIMEOUT_MS);

  printf("init complete ...\n");

  while (true) {

    if (cyw43_wifi_link_status(&cyw43_state, CYW43_ITF_STA) != CYW43_LINK_JOIN)
      cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASS, CYW43_AUTH_WPA2_AES_PSK, WIFI_CONNECT_TIMEOUT_MS);

    if (!mqtt.is_connected())
      mqtt.connect(MQTT_BROKER_IP, 1883);

    while (rf_recv.newDataAvailable()) {
      sensorData_t newSensorData = rf_recv.popDataFromBuffer();

      if (newSensorData == lastSensorData) {
        std::string id = std::to_string(newSensorData.id);
        std::string topic = "/home/windows/" + id;

        std::string payload = std::to_string(newSensorData.isOpen);

        mqtt.publish(topic, payload);
      }
      // printf("Sensor ID:%d Val:%d\n", newSensorData.id, newSensorData.isOpen);
      lastSensorData = newSensorData;
    }

    LwipMQTT::mqtt_message_t message;
    while (mqtt.message_is_available()) {
      message = mqtt.read_message();
      printf("%s - %s\r\n", message.topic.c_str(), message.payload.c_str());
    }

    blink_led();
  }

  return 0;
}

void blink_led() {
  cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
  sleep_ms(250);
  cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
  sleep_ms(750);
}
