/**
 * @file rf_ask_driver.h
 * @author Michael Hammerl (michael.hammerl@stud.th-rosenheim.de)
 * @brief
 * @version 0.1
 * @date 2023-04-20
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef RF_ASK_DRIVER_H
#define RF_ASK_DRIVER_H

#include "sensor_data.h"
#include <cstdint>
#include <bitset>
#include <queue>
#include <array>

#define BUF_LEN 32

class RfAskDriver
{
public:
  /**
   * @brief Construct a new Rf Ask Driver object
   *
   * @param rxPin Data Pin of RF Receiver
   */
  RfAskDriver(const uint16_t rxPin);
  ~RfAskDriver();

  /**
   * @brief Start receiving sensor data into the buffer
   *
   */
  void startReceiving();

    /**
   * @brief Stop receiving sensor data
   *
   */
  void stopReceiving();

  /**
   * @brief Check if new Data from the Sensor is in the buffer
   *
   * @return true
   * @return false
   */
  static bool newDataAvailable();

  /**
   * @brief Get and delete last element from buffer
   *
   * @return sensorData_t
   */
  static sensorData_t popDataFromBuffer();

private:
  // describe high and low time of a single bit in us
  typedef struct {
    uint16_t high;
    uint16_t low;
  } highLow_t;

  typedef struct {
    highLow_t zero;
    highLow_t one;
    uint16_t syncLength;  // lenth of low level before data starts in us
    uint16_t numOfBits;
  } protocol_t;

  static constexpr protocol_t _protocol {
    { 525, 1325 }, // zero
    { 1425, 425 }, // one
      14000,       // syncLength
      24           // numOfBits -> throw away last bit (0) -> causes problems because of the sync pulse afterwards
  };
  // 255 == 100% -> avoid using floats for percentage calculation
  static constexpr uint8_t _receivingTolerance = 20;

  // data pin the receiver is connected to
  const uint16_t _rxPin;

  // save timings for debugging purposes
  inline static std::array<highLow_t, BUF_LEN> _timings;
  // bit buffer
  inline static std::bitset<BUF_LEN> _rxBuf;
  // parsed sensor data circular buffer
  inline static std::queue<sensorData_t> _sensorDataBuf;

  /**
   * @brief Parse Data from _rxBuf[] to _sensorDataBuf[]
   */
  static void parseBuffer();

  /**
   * @brief Interrupt handler to get timings of signal's rising/falling edges
   *
   * @param gpio GPIO number
   * @param events Which events caused this interrupt. See \ref gpio_irq_level for details.
   */
  static void handleInterrupt(uint gpio, uint32_t events);

  /**
   * @brief Check if a and b are equal (within "_receivingTolerance")
   *
   * @param a const uint32_t&
   * @param b const uint32_t&
   * @return true
   * @return false
   */
  static bool areEqualWithTolerance(const uint32_t& a, const uint32_t& b);

    /**
   * @brief Check if (a.low == b.low) && (a.high == b.high) (within "_receivingTolerance")
   *
   * @param a const highLow_t&
   * @param b const highLow_t&
   * @return true
   * @return false
   */
  static bool areEqualWithTolerance(const highLow_t& a, const highLow_t& b);
};

#endif /* RF_ASK_DRIVER_H */
