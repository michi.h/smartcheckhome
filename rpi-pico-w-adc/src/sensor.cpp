#include "sensor.h"
#include "pico/time.h"
#include <cmath>

Sensor::Sensor(ads1115_adc_t& adc, ads1115_mux_t mux)
  : _adc(&adc)
  , _mux(mux) {

    // calculate adc dc offset
    uint64_t adc_offset = 0;
    // fill _buf with adc values
    this->read();
    for(auto& val : _buf) {
      // sum up _buf values
      adc_offset += val;
    }
    // get mean value
    _dc_offset = adc_offset / NUM_OF_SAMPLES;
}

double Sensor::read() {
  uint16_t adc_value;

  _new_value_rdy = false;

  ads1115_set_pga(ADS1115_PGA_6_144, _adc);
  ads1115_set_data_rate(ADS1115_RATE_860_SPS, _adc);
  ads1115_set_operating_mode(ADS1115_MODE_CONTINUOUS, _adc);
  ads1115_set_input_mux(_mux, _adc);
  ads1115_write_config(_adc, false);

  // Sample adc voltage with SAMPLE_FREQ
  repeating_timer_t timer;
  add_repeating_timer_us(-1000000 / SAMPLE_FREQ, timer_callback, _adc, &timer);

  while (!_new_value_rdy); // wait for new _adc_v_rms value

  cancel_repeating_timer(&timer);

  _new_value_rdy = false;

  return _adc_v_rms;
}

bool Sensor::timer_callback(repeating_timer_t* rt) {
  ads1115_adc* adc = (ads1115_adc*)rt->user_data;

  static uint8_t i = 0;
  static int64_t adc_sum = 0;

  ads1115_read_adc(&_buf[i], adc);

  // substract dc offset
  int32_t centered_buf = _buf[i] - _dc_offset;

  // rms calculation
  centered_buf = pow(centered_buf, 2.0);
  adc_sum += centered_buf; // add up values

  i++; // increment index

  if (i >= NUM_OF_SAMPLES) {

    _adc_v_rms = ads1115_raw_to_volts(sqrt(adc_sum / NUM_OF_SAMPLES), adc); // calculate mean rms voltage

    cancel_repeating_timer(rt);
    _new_value_rdy = true;
    adc_sum = 0;
    i = 0;
  }

  return true; // keep repeating
}
