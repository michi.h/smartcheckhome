#include "adc.h"
#include "pico/stdlib.h"

ADC::ADC(const uint8_t num_of_inputs, const uint8_t sda_pin, const uint8_t scl_pin, const uint8_t i2c_addr, i2c_inst_t* i2c_port)
  : _num_of_sensors(num_of_inputs) {
    // Initialise I2C
  i2c_init(i2c_port, I2C_FREQ);
  gpio_set_function(sda_pin, GPIO_FUNC_I2C);
  gpio_set_function(scl_pin, GPIO_FUNC_I2C);
  gpio_pull_up(sda_pin);
  gpio_pull_up(scl_pin);

  // Initialize ADS1115
  ads1115_init(i2c_port, i2c_addr, &_adc);

  for (size_t i = 0; i < num_of_inputs; i++) {
    _sensors[i] = std::make_unique<Sensor>(_adc, _mux[i]);
  }

}

ADC::~ADC() {}

void ADC::read(std::array<float, 4>& value) {
  for (size_t i = 0; i < _num_of_sensors; i++) {
    value[i] = _sensors[i]->read();
  }
}

uint8_t ADC::get_num_of_sensors() const {
  return _num_of_sensors;
}
