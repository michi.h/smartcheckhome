#include "constants.h"
#include "lwip_mqtt.h"
#include "adc.h"
#include "pico/cyw43_arch.h"
#include "pico/stdlib.h"
#include "pico/float.h"

#include <string>
#include <array>

#define CONVERSION_FACTOR 2602.152955f // 8 * sqrt(2) * 230 V

#define I2C_PORT i2c0
#define I2C_FREQ 400000
#define ADS1115_I2C_ADDR1 0x48
#define ADS1115_I2C_ADDR2 0x49
const uint8_t SDA_PIN = 20;
const uint8_t SCL_PIN = 21;

void blink_led();

int main() {
  stdio_init_all();

  cyw43_arch_init_with_country(CYW43_COUNTRY_GERMANY);
  cyw43_arch_enable_sta_mode();
  cyw43_wifi_pm(&cyw43_state, CYW43_PERFORMANCE_PM);

  LwipMQTT mqtt = LwipMQTT(
      "PiPicoADC",      /* client id*/
      "mqtt",           /* user */
      "C4C7UZhQVFWQMG"  /* pass */
  );

  // Initialise ADC
  std::array<ADC, 2> adc =
    { ADC(1, SDA_PIN, SCL_PIN, ADS1115_I2C_ADDR1, I2C_PORT),
      ADC(1, SDA_PIN, SCL_PIN, ADS1115_I2C_ADDR2, I2C_PORT) };

  // Data containers
  float volts = 0;

  cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASS, CYW43_AUTH_WPA2_AES_PSK, WIFI_CONNECT_TIMEOUT_MS);

  printf("init complete ...\n");

  while (true) {

    if (cyw43_wifi_link_status(&cyw43_state, CYW43_ITF_STA) != CYW43_LINK_JOIN)
      cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASS, CYW43_AUTH_WPA2_AES_PSK, WIFI_CONNECT_TIMEOUT_MS);

    if (!mqtt.is_connected())
      mqtt.connect(MQTT_BROKER_IP, 1883);

    // Read sensor values and print
    std::array<float, 4> volts_buffer;
    float power;

    uint16_t id_adc = 1;
    for(auto& a : adc) {
      a.read(volts_buffer);

      for (size_t i = 0; i < a.get_num_of_sensors(); i++) {
        power = volts_buffer[i] * CONVERSION_FACTOR;
        printf("ADC Voltage RMS: %f\n", volts_buffer[i]);
        printf("Power: %f\n" , power);
        std::string topic = "/home/power/" + std::to_string(id_adc) + std::to_string(i);
        mqtt.publish(topic, std::to_string(power));
      }
      id_adc++;
    }

    sleep_ms(4000);

    blink_led();
  }

  return 0;
}

void blink_led() {
  cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
  sleep_ms(250);
  cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
  sleep_ms(750);
}
