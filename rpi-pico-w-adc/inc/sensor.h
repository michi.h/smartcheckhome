#ifndef SENSOR_H
#define SENSOR_H

#include "ads1115.h"
#include <array>

const uint8_t NUM_OF_SAMPLES = 128;
const uint16_t SAMPLE_FREQ = 128; // matched to ADS1115 datarate setting

class Sensor
{
public:
  /**
   * @brief Construct a new Sensor object
   *
   * @param adc reference to the adc
   * @param mux ads1115 input the sensor is connected to
   */
  Sensor(ads1115_adc_t& adc, ads1115_mux_t mux);

  /**
   * @brief Read adc input the sensor is connected to.
   *
   * @return double adc rms voltage
   */
  double read();

private:
  /**
   * @brief // Reference to the adc
   */
  ads1115_adc_t* _adc;

  /**
   * @brief Input multiplexer configuration for sensor channel
   */
  const ads1115_mux_t _mux;

  /**
   * @brief DC offset of AC signal
   */
  volatile inline static uint16_t _dc_offset;

  /**
   * @brief Buffer for adc values
   */
  inline static std::array<uint16_t, NUM_OF_SAMPLES> _buf;

  /**
   * @brief Flag to indicate new value is ready
   */
  volatile inline static bool _new_value_rdy;

  /**
   * @brief New value
   */
  volatile inline static double _adc_v_rms;

  /**
   * @brief Samples adc input (set by _mux) periodically, returns mean rms over all samples in _new_v_rms and sets _new_value_rdy flag
   *
   * @param rt ads1115_adc*
   */
  static bool timer_callback(repeating_timer_t* rt);
};


#endif /* SENSOR_H */
