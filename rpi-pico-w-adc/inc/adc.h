#ifndef ADC_H
#define ADC_H

#include "ads1115.h"
#include "sensor.h"
#include <array>
#include <memory>

#define I2C_FREQ 400000

class ADC
{
public:
  /**
   * @brief Construct a new ADC object
   *
   * @param num_of_inputs Number of inputs being used (2 -> input 0 and 1 active)
   * @param sda_pin i2c sda pin adc is connected to
   * @param scl_pin i2c scl pin the adc is connected to
   * @param i2c_addr i2c address of the adc
   * @param i2c_port i2c scl port the adc is connected to
   */
  ADC(const uint8_t num_of_inputs, const uint8_t sda_pin, const uint8_t scl_pin, const uint8_t i2c_addr, i2c_inst_t* i2c_port);

  ~ADC();

  /**
   * @brief read Value from sensors into array
   *
   * @param value array to read data into
   */
  void read(std::array<float, 4>& value);

  /**
   * @brief get the number of sensors
   *
   */
  uint8_t get_num_of_sensors() const;

private:
  /**
   * @brief holds the ads1115_mux_t values for each adc input
   *
   */
  static constexpr ads1115_mux_t _mux[4] = {
    ADS1115_MUX_SINGLE_0,
    ADS1115_MUX_SINGLE_1,
    ADS1115_MUX_SINGLE_2,
    ADS1115_MUX_SINGLE_3
  };

  struct ads1115_adc _adc;
  std::array<std::unique_ptr<Sensor>, 4> _sensors;
  const uint8_t _num_of_sensors;
};

#endif /* ADC_H */
